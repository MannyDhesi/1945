# README #

## Summary ##

This project is a remake of the classic game, 1945, which is a 2D scrolling shooter. This game uses the object oriented method and includes such things as a random enemy spawner, automatic difficulty increase, HUD including health bar and much more.

## Contents of the project ##

There are three folders within this project to allow the game to be played/tested in multiple ways. 

### 1945 - Executable ###
This folder contains a version of the game that can be played directly by the means of an exe file titled "1945.exe".

### 1945 - Installer ###
This folder contains a version of the game that can be installed to the user's machine in the same way as more mainstream games. The game can be uninstalled by using the system's normal uninstall utility.

### 1945 - Solution ###
This folder contains the project solution, containing all source code and resources used to create the game. 

## How do I get set up? ##

This application has been created and tested using Microsoft Visual Studio 2010 in Windows. Compatibility with other compilers and/or operating systems cannot be commented on.

This game is created using XNA, meaning the XNA Game Studio may be required in order to run the application. This can be downloaded from [here](http://www.microsoft.com/en-us/download/details.aspx?id=23714).