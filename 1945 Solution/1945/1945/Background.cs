﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace _1945
{
    static class Background
    {
        #region Declarations

        static public Vector2 waterStartPosition = new Vector2(0, -64);
        static public Vector2 waterEndPosition = new Vector2(0, -1);
        static public Vector2 waterCurrentPosition;
        static private Texture2D water;

        static public List<Vector2> islandStartPosition = new List<Vector2>();
        static public int islandEndY;
        static public List<Vector2> islandCurrentPosition = new List<Vector2>();
        static private List<Rectangle> islands = new List<Rectangle>();
        static private Texture2D spriteSheet;

        static private Random rand = new Random();

        #endregion

        #region Initialization

        static public void Initialize(Texture2D WaterBackground, Texture2D SpriteSheet)
        {
            // Set the textures and position for the water background
            water = WaterBackground;
            waterCurrentPosition = waterStartPosition;

            // Set the texture for the spritesheet, used for the islands
            spriteSheet = SpriteSheet;

            GenerateIslands();
        }

        #endregion

        #region Generate Islands

        static public void GenerateIslands()
        {
            // Create the rectangle position on the sprite sheet
            // for each island
            islands.Clear();
            islands.Add(new Rectangle(224, 0, 64, 64));
            islands.Add(new Rectangle(288, 0, 64, 64));
            islands.Add(new Rectangle(352, 0, 64, 64));

            // Set the start position for each of the islands
            islandStartPosition.Clear();
            islandStartPosition.Add(new Vector2(rand.Next(50, 750), 0));
            islandStartPosition.Add(new Vector2(rand.Next(50, 750), 200));
            islandStartPosition.Add(new Vector2(rand.Next(50, 750), 400));

            // Set the end Y position for the islands
            islandEndY = 600;

            for (int i = 0; i < islands.Count; i++)
            {
                islandCurrentPosition.Add(islandStartPosition[i]);
            }
        }

        #endregion

        #region Update and Draw

        static public void Update(GameTime gametime)
        {
            // If the water background has moved down enough, reset the position
            // This gives the visual effect of a continuously scrolling background
            if (waterCurrentPosition.Y < waterEndPosition.Y)
                waterCurrentPosition.Y++;
            else
                waterCurrentPosition = waterStartPosition;

            // If the island has reached the bottom of the screen, reset the position to:
            // X = randomly across the screen, Y = just above the top of the screen
            for (int i = 0; i < islands.Count; i++)
            {
                if (islandCurrentPosition[i].Y < islandEndY)
                    islandCurrentPosition[i] = new Vector2(islandCurrentPosition[i].X, islandCurrentPosition[i].Y + 1);

                else
                    islandCurrentPosition[i] = new Vector2(rand.Next(50, 750), -64);
            }
        }

        static public void Draw(SpriteBatch spriteBatch)
        {
            // Draw the water background
            spriteBatch.Draw(water, new Rectangle((int)waterCurrentPosition.X, (int)waterCurrentPosition.Y, 800, 672), Color.White);

            // Draw each island
            for (int i = 0; i < islands.Count; i++)
            {
                spriteBatch.Draw(spriteSheet, islandCurrentPosition[i], islands[i], Color.White);
            }
        }

        #endregion
    }
}
