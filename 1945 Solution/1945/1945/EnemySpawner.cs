﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace _1945
{
    class EnemySpawner
    {
        #region Declarations

        public static float LastSpawnCounter = 0;
        public static float minSpawnTime = 1.0f;
        private static int spawnType;
        public static int MaxActiveEnemies = 1;
        public static int CurrentEnemyLevel = 1;

        private static Random rand = new Random(); 

        #endregion

        #region Public Methods

        public static void Update(GameTime gameTime)
        {
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;
            LastSpawnCounter += elapsed;

            // If the set time has passed since the last enemy spawn, spawn another
            if (LastSpawnCounter > minSpawnTime)
            {
                // When the score level is reached, add the next type of enemy
                if (GameManager.Score >= 50 && GameManager.Score < 100)
                    CurrentEnemyLevel = 2;

                if (GameManager.Score >= 100 && GameManager.Score < 150)                
                    CurrentEnemyLevel = 3;

                if (GameManager.Score >= 150 && GameManager.Score < 200)
                    CurrentEnemyLevel = 4;

                // When the score level is reached, increase the max enemies
                if (GameManager.Score >= 20 && GameManager.Score < 50)
                    MaxActiveEnemies = 2;

                if (GameManager.Score >= 50 && GameManager.Score < 100)
                    MaxActiveEnemies = 3;

                if (GameManager.Score >= 100 && GameManager.Score < 200)
                    MaxActiveEnemies = 4;

                if (GameManager.Score >= 200)
                    MaxActiveEnemies = 5;

                // If the enemies on screen is less than the max allowed
                if (EnemyManager.Enemies.Count < MaxActiveEnemies)
                {
                    // Add a random type of enemy
                    spawnType = rand.Next(1, CurrentEnemyLevel + 1);

                    switch (spawnType)
                    {
                        case 1:
                            EnemyManager.AddEnemy1();
                            break;
                        case 2:
                            EnemyManager.AddEnemy2();
                            break;
                        case 3:
                            EnemyManager.AddEnemy3();
                            break;
                        case 4:
                            EnemyManager.AddEnemy4();
                            break;
                    }

                    // Reset the spawn counter
                    LastSpawnCounter = 0;
                }

            }
        }

        #endregion
    }
}
