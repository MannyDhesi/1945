﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace _1945
{
    class Sprite
    {
        #region Declarations

        // Stores all of the animation frames for any individual sprite
        public Texture2D Texture;

        private Vector2 worldLocation = Vector2.Zero;
        private Vector2 velocity = Vector2.Zero;

        // Holds a single Rectangle object for each animation frame defined for the sprite
        private List<Rectangle> frames = new List<Rectangle>();

        // Controls the animation times
        private int currentFrame;
        private float frameTime = 0.1f;
        private float timeForCurrentFrame = 0.0f;

        // Used to change the color of the sprite
        private Color tintColour = Color.White;

        // Stores the current rotation
        private float rotation = 0.0f;

        // If set to true, the sprite will not be updated or drawn
        public bool Expired = false;
        // If set to false, the sprite will not advance frame animations
        public bool Animate = true;
        // If set to false, the sprite will not advance its frame animations if the velocity
        // is equal to Vector2.Zero. This means the sprite will only animate when moving
        public bool AnimateWhenStopped = true;

        // Collision variables
        public bool Collidable = true;
        public int CollisionRadius = 0;
        public int BoundingXPadding = 0;
        public int BoundingYPadding = 0;

        #endregion

        #region Constructors

        public Sprite(
            Vector2 worldLocation, 
            Texture2D texture, 
            Rectangle initialFrame, 
            Vector2 velocity)
        {
            this.worldLocation = worldLocation;
            this.Texture = texture;
            this.velocity = velocity;

            frames.Add(initialFrame);
        }

        #endregion

        #region Drawing and Animation Properties

        // Simple pass-through for their underlying private members as no additional code 
        // or checks need to be done when these are manipulated
        public int FrameWidth
        {
            get { return frames[0].Width; }
        }

        // Simple pass-through for their underlying private members as no additional code 
        // or checks need to be done when these are manipulated
        public int FrameHeight
        {
            get { return frames[0].Height; }
        }

        // Simple pass-through for their underlying private members as no additional code 
        // or checks need to be done when these are manipulated
        public Color TintColour
        {
            get { return tintColour; }
            set { tintColour = value; }
        }

        // When Rotation is set, the value is divided by MathHelper.TwoPi and the 
        // remainder of the result is stored in the rotation member!
        // With this way all values will be kept between 0 and 2*pi. 
        public float Rotation
        {
            get { return rotation; }
            set { rotation = value % MathHelper.TwoPi; }
        }

        // The Frame property uses the MathHelper.Clamp() to ensure that when it 
        // is set the value stored in currentFrame is valid for the frames list of 
        // Rectangles. 
        // EG: This will prevent setting the number of frame 10 while the sprite has 
        // only 5 animation frames!
        public int Frame
        {
            get { return currentFrame; }
            set { currentFrame = (int)MathHelper.Clamp(value, 0, frames.Count - 1); }
        }

        // The FrameTime property allows the speed at which the animation plays to 
        // be updated. 
        // A FrameTime = 0 will result in an animation that updates its frame during 
        // every Update() cycle!
        public float FrameTime
        {
            get { return frameTime; }
            set { frameTime = MathHelper.Max(0, value); }
        }

        // The Rectangle Source property returns the Rectangle associated with the 
        // current frame from the frames list.
        public Rectangle Source
        {
            get { return frames[currentFrame]; }
        }

        #endregion

        #region Positional Properties

        public Vector2 WorldLocation
        {
            get { return worldLocation; }
            set { worldLocation = value; }
        }

        public Vector2 Velocity
        {
            get { return velocity; }
            set { velocity = value; }
        }

        public Rectangle WorldRectangle
        {
            get
            {
                return new Rectangle(
                    (int)worldLocation.X,
                    (int)worldLocation.Y,
                    FrameWidth,
                    FrameHeight);
            }
        }

        public Vector2 RelativeCentre
        {
            get { return new Vector2(FrameWidth / 2, FrameHeight / 2); }
        }

        public Vector2 WorldCentre
        {
            get { return worldLocation + RelativeCentre; }
        }

        #endregion

        #region Collision Related Properties

        // Both of these functions check whether the sprite is on the Visible World area.
        // If not, then the collision calculations will not take part.
        // This minimises the memory usage.

        // When checking for a bounding box collision, this function accepts another
        // bounding box and returns true if the two overlap at any point
        public bool IsBoxColliding(Rectangle otherBox)
        {
            if ((Collidable) && (!Expired))
            {
                return BoundingBoxRect.Intersects(otherBox);
            }

            else
            {
                return false;
            }
        }

        // Accepts a Vector2 which represents the centre of the object the sprite will be
        // compared against, and the other objects radius.
        // Returns true if the distance between the two centres is less than the sum of the two radii
        public bool IsCircleColliding(Vector2 otherCentre, float otherRadius)
        {
            if ((Collidable) && (!Expired))
            {
                if (Vector2.Distance(WorldCentre, otherCentre) < (CollisionRadius + otherRadius))
                    return true;

                else
                    return false;
            }

            else
            {
                return false;
            }
        }

        #endregion

        #region Collision Detection Methods

        // Provides a rectangle object equivalent to the location and size of the sprite,
        // accounting for the padding values around the edges
        public Rectangle BoundingBoxRect
        {
            get
            {
                return new Rectangle(
                    (int)worldLocation.X + BoundingXPadding,
                    (int)worldLocation.Y + BoundingYPadding,
                    FrameWidth - (BoundingXPadding * 2),
                    FrameHeight - (BoundingYPadding * 2));
            }
        }

        #endregion

        #region Animation-Related Methods

        // Adds a frame to the sprite's animation by simply adding the corresponding
        // rectangle to the frames list
        public void AddFrame(Rectangle frameRectangle)
        {
            frames.Add(frameRectangle);
        }

        public void RotateTo(Vector2 direction)
        {
            Rotation = (float)Math.Atan2(direction.Y, direction.X);
        }

        #endregion

        #region Update and Draw Methods

        public virtual void Update(GameTime gameTime)
        {
            // Check if the animation is not expired (active)
            if (!Expired)
            {
                // Returns the amount of time from the previous update
                float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;

                // updates the New time set for the new animation to start calculating
                // the next elapsed time
                timeForCurrentFrame += elapsed;

                // Checks if the current sprite is animating and play the animation
                if (Animate)
                {
                    if (timeForCurrentFrame >= frameTime)
                    {
                        if ((AnimateWhenStopped) || (velocity != Vector2.Zero))
                        {
                            currentFrame = (currentFrame + 1) % (frames.Count);
                            timeForCurrentFrame = 0.0f;
                        }
                    }
                }

                // Determines the distance moved over a single frame
                worldLocation += (velocity * elapsed);
            }
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            if (!Expired)
            {
                    spriteBatch.Draw(
                        Texture,
                        WorldLocation,
                        Source,
                        tintColour,
                        rotation,
                        RelativeCentre,
                        1.0f,
                        SpriteEffects.None,
                        0.0f);
            }
        }

        #endregion
    }
}
