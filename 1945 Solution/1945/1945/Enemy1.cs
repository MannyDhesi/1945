﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace _1945
{
    class Enemy1 : Enemy
    {
        #region Declarations

        static public Rectangle initialFrame = new Rectangle(0, 0, 32, 32);

        #endregion

        #region Constructor

        public Enemy1(Vector2 worldLocation,
            Texture2D texture)
        {
            sprite = new Sprite(
               worldLocation,
               texture,
               initialFrame,
               Vector2.Zero);

            speed = 100f;
            health = 20;
            score = 10;

            sprite.AnimateWhenStopped = true;
            sprite.CollisionRadius = 16;

            // Initialize the size of the sprite
            int frameWidth = initialFrame.Width;
            int frameHeight = initialFrame.Height;

            // Add each frame for the animation of the base sprite
            for (int x = 1; x < 3; x++)
            {
                // Sets a new rectangle to encapsulate the tank base so everytime we move
                // to the next frame (x++) the new rectangle will be pushed to the new (X,Y).
                // On the sprite sheet, the tank sprites are set up along the same Y value
                // which is why the baseInitialFrame.Y remains the same for all frames
                sprite.AddFrame(new Rectangle(initialFrame.X + (frameHeight * x),
                    initialFrame.Y, frameWidth, frameHeight));
            }
        }

        #endregion

        #region Public Methods

        public override void Update(GameTime gameTime)
        {
            // If the enemy is not destroyed
            if (!Destroyed)
            {
                // Move their plane in the velocity's direction
                Vector2 direction = new Vector2(0, 1);
                direction.Normalize();

                sprite.Velocity = direction * speed;
                sprite.RotateTo(direction);

                // If their plane has left the screen, destroy it
                if (sprite.WorldLocation.Y > 600)
                    Destroyed = true;
            }

            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }

        #endregion
    }
}
