﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace _1945
{
    static class TileMap
    {
        #region Declarations
        public const int tileWidth = 32;
        public const int tileHeight = 32;
        public const int mapWidth = 50;
        public const int mapHeight = 50;

        public const int floorTileStart = 0;
        public const int floorTileEnd = 3;
        public const int wallTileStart = 4;
        public const int wallTileEnd = 7;

        static private Texture2D texture;
        static private List<Rectangle> tiles = new List<Rectangle>();
        static private int[,] mapSquares = new int[mapWidth, mapHeight];
        static private Random rand = new Random();
        #endregion

        #region Initialization

        static public void Initialize(Texture2D tileTexture)
        {
            // Set the texture
            texture = tileTexture;
            // Clear the existing tiles list and add 8 tiles
            tiles.Clear();
            tiles.Add(new Rectangle(267, 201, tileWidth, tileHeight));

            for (int x = 0; x < mapWidth; x++)
            {
                for (int y = 0; y < mapHeight; y++)
                {
                    // Sets each square in mapSquares array to the 
                    // first of the floor tile index
                    // This way we start with a known, empty map
                    mapSquares[x, y] = floorTileStart;
                }
            }

            GenerateRandomMap();
        }

        #endregion

        #region Information about Map Squares

        // Converts from world-based pixel coordinates to map square references
        // Done by giving a pixel reference in real Gamew world [x,y] and divide
        // its x,y coordinates by the tileWidth/Height to map it to the new MapWorld
        static public int GetSquareByPixelX(int pixelX)
        {
            return pixelX / tileWidth;
        }

        static public int GetSquareByPixelY(int pixelY)
        {
            return pixelY / tileHeight;
        }

        // Makes sure the new map values will be set as a vector now to reference
        // it later on to the mapSquares[] array. Therefore this vector must contain
        // x,y coordinates in range 0-49
        static public Vector2 GetSquareAtPixel(Vector2 pixelLocation)
        {
            return new Vector2(GetSquareByPixelX((int)pixelLocation.X),
                GetSquareByPixelY((int)pixelLocation.Y));
        }

        // Returns the centre of the tiles passed though
        // Useful for enemy movement as they will be forced to move toward the
        // centre of each tile
        static public Vector2 GetSquareCentre(int squareX, int squareY)
        {
            return new Vector2((squareX * tileWidth) + (tileWidth / 2),
                (squareY * tileHeight) + (tileHeight / 2));
        }

        static public Vector2 GetSquareCentre(Vector2 square)
        {
            return GetSquareCentre((int)square.X, (int)square.Y);
        }

        // Checks what pixels on the world map this current square occupies.
        // Needed for the collisions with a wall on the map
        static public Rectangle SquareWorldRectangle(int x, int y)
        {
            return new Rectangle(x * tileWidth, y * tileHeight, tileWidth, tileHeight);
        }

        static public Rectangle SquareWorldRectangle(Vector2 square)
        {
            return SquareWorldRectangle((int)square.X, (int)square.Y);
        }

        // Provides the same information as SquareWorldRectangle but gives the
        // local coordinates.
        static public Rectangle SquareScreenRectangle(int x, int y)
        {
            return Camera.Transform(SquareWorldRectangle(x, y));
        }

        static public Rectangle SquareScreenRectangle(Vector2 square)
        {
            return SquareScreenRectangle((int)square.X, (int)square.Y);
        }

        #endregion

        #region Information about Map Tiles

        // Determines the tile index with any particular square on the map
        static public int GetTileAtSquare(int tileX, int tileY)
        {
            if ((tileX >= 0) && (tileX < mapWidth) && (tileY >= 0) && (tileY < mapHeight))
            {
                return mapSquares[tileX, tileY];
            }

            else
            {
                return -1;
            }
        }

        // Allows the index of any square to be changed
        static public void SetTileAtSquare(int tileX, int tileY, int tile)
        {
            if ((tileX >= 0) && (tileX < mapWidth) && (tileY >= 0) && (tileY < mapHeight))
            {
                mapSquares[tileX, tileY] = tile;
            }
        }

        // Combines both GetTileAtSquare and SetTileAtSquare
        // Provides more convenient access to tile information
        static public int GetTileAtPixel(int pixelX, int pixelY)
        {
            return GetTileAtSquare(GetSquareByPixelX(pixelX), GetSquareByPixelY(pixelY));
        }

        static public int GetTileAtPixel(Vector2 pixelLocation)
        {
            return GetTileAtPixel((int)pixelLocation.X, (int)pixelLocation.Y);
        }

        // Checks the contents of a given square
        // Returns true if the tile index is greater than or equal to the first
        // defined wall tile index (wallTileStart)
        static public bool IsWallTile(int tileX, int tileY)
        {
            int tileIndex = GetTileAtSquare(tileX, tileY);

            if (tileIndex == -1)
            {
                return false;
            }

            return tileIndex >= wallTileEnd;
        }

        static public bool IsWallTile(Vector2 square)
        {
            return IsWallTile((int)square.X, (int)square.Y);
        }

        static public bool IsWallTileByPixel(Vector2 pixelLocation)
        {
            return IsWallTile(GetSquareByPixelX((int)pixelLocation.X), 
                GetSquareByPixelY((int)pixelLocation.Y));
        }

        #endregion

        #region Drawing

        static public void Draw(SpriteBatch spriteBatch)
        {
            // Integers used to control the loop
            // Start X and Y are the top right coordinates of the camera
            // End X and Y are the bottom right coordinates of the camera
            // If the tile in in view of the camera, it will be drawn
            int startX = GetSquareByPixelX((int)Camera.Position.X);
            int endX = GetSquareByPixelX((int)Camera.Position.X + Camera.ViewPortWidth);

            int startY = GetSquareByPixelY((int)Camera.Position.Y);
            int endY = GetSquareByPixelY((int)Camera.Position.Y + Camera.ViewPortHeight);

            for (int x = startX; x <= endX; x++)
            {
                for (int y = startY; y <= endY; y++)
                {
                    if ((x >= 0) && (y >= 0) && (x < mapWidth) && (y < mapHeight))
                    {
                        spriteBatch.Draw(texture, 
                            SquareScreenRectangle(x, y), 
                            tiles[GetTileAtSquare(x, y)], 
                            Color.White);
                    }
                }
            }
        }

        #endregion

        #region MAP Generation

        static public void GenerateRandomMap()
        {
            int wallChancePerSquare = 10;
            
            // Selects a floor and wall tile to be used on the current map (random)
            int floorTile = rand.Next(floorTileStart, floorTileEnd + 1);
            int wallTile = rand.Next(wallTileStart, wallTileEnd + 1);

            for (int x = 0; x < mapWidth; x++)
            {
                for (int y = 0; y < mapHeight; y++)
                {
                    // Sets each tile to the floor tile initally
                    mapSquares[x, y] = floorTile;

                    // If the tile is on the outside edge, it is set to be a wall
                    if ((x == 0) || (y == 0) || (x == mapWidth - 1) || (y == mapHeight - 2))
                    {
                        mapSquares[x, y] = wallTile;
                        continue;
                    }

                    // If the tile is one in from the outside edge, set to stay as a floor
                    if ((x == 1) || (y == 1) || (x == mapWidth - 2) || (y == mapHeight - 2))
                    {
                        continue;
                    }

                    // if the tile is anywhere else, let the game randomly decide whether it
                    // is a floor or a wall tile
                    if (rand.Next(0, 100) <= wallChancePerSquare)
                    {
                        mapSquares[x, y] = wallTile;
                    }
                }
            }
        }

        #endregion
    }
}
