﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace _1945
{
    static class Player
    {
        #region Declarations

        // Declare a sprite for the plane
        public static Sprite sprite;
        public static int Health = 100;

        #endregion

        #region Initialization

        public static void Initialize(Texture2D texture,
            Rectangle initialFrame, int frameCount,
            Vector2 worldLocation)
        {
            // Initialize the size of the sprite
            int frameWidth = initialFrame.Width;
            int frameHeight = initialFrame.Height;

            // Creates a new sprite based on the parameters passed through from Game1.cs
            sprite = new Sprite(worldLocation, texture, initialFrame, Vector2.Zero);
            sprite.AnimateWhenStopped = true;
            sprite.CollisionRadius = 32;

            // Add each frame for the animation of the base sprite
            for (int x = 1; x < frameCount; x++)
            {
                // Sets a new rectangle to encapsulate the tank base so everytime we move
                // to the next frame (x++) the new rectangle will be pushed to the new (X,Y).
                // On the sprite sheet, the tank sprites are set up along the same Y value
                // which is why the baseInitialFrame.Y remains the same for all frames
                sprite.AddFrame(new Rectangle(initialFrame.X + (frameHeight * x),
                    initialFrame.Y, frameWidth, frameHeight));
            }
        }

        #endregion

        #region Input Handling

        // Check the movement controls from the keyboard
        private static Vector2 handleKeyboardMovement(KeyboardState keyState)
        {
            Vector2 keyMovement = Vector2.Zero;
            if (keyState.IsKeyDown(Keys.W))
                keyMovement.Y--;

            if (keyState.IsKeyDown(Keys.A))
                keyMovement.X--;

            if (keyState.IsKeyDown(Keys.S))
                keyMovement.Y++;

            if (keyState.IsKeyDown(Keys.D))
                keyMovement.X++;

            return keyMovement;
        }

        // Check the movement controls from the gamepad
        private static Vector2 handleGamePadMovement(GamePadState gamepadState)
        {
            return new Vector2(
                gamepadState.ThumbSticks.Left.X,
                -gamepadState.ThumbSticks.Left.Y);
        }

        // Check the shooting controls from the keyboard
        private static Vector2 handleKeyboardShots(KeyboardState keyState)
        {
            Vector2 keyShots = Vector2.Zero;

            if (keyState.IsKeyDown(Keys.Space))
                keyShots = new Vector2(0, -1);

            if (keyState.IsKeyDown(Keys.Z))
            {
                if (WeaponManager.CanFireRocket)
                    WeaponManager.FireRocket(
                            new Vector2(sprite.WorldLocation.X, sprite.WorldLocation.Y - 32),
                             new Vector2(0, -1) * WeaponManager.WeaponSpeed);
            }

            return keyShots;
        }

        // Check the shooting controls from the gamepad
        private static Vector2 handleGamePadShots(GamePadState gamepadState)
        {
            Vector2 keyShots = Vector2.Zero;

            if (gamepadState.Buttons.A == ButtonState.Pressed)
                keyShots = new Vector2(0, -1);

            if (gamepadState.Buttons.B == ButtonState.Pressed)
            {
                if (WeaponManager.CanFireRocket)
                    WeaponManager.FireRocket(
                            new Vector2(sprite.WorldLocation.X, sprite.WorldLocation.Y - 32),
                            new Vector2(0, -1) * WeaponManager.WeaponSpeed);
            }

            return keyShots;
        }

        // Create the Vectors for both moving and shooting
        private static void handleInput(GameTime gameTime)
        {
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;

            Vector2 moveAngle = Vector2.Zero;
            Vector2 fireAngle = Vector2.Zero;

            moveAngle += handleKeyboardMovement(Keyboard.GetState());
            moveAngle += handleGamePadMovement(GamePad.GetState(PlayerIndex.One));

            fireAngle += handleKeyboardShots(Keyboard.GetState());
            fireAngle += handleGamePadShots(GamePad.GetState(PlayerIndex.One));

            // If the player is moving, the sound is louder
            if (moveAngle != Vector2.Zero)
                SoundManager.soundPlaneInstance.Volume = 0.8f;

            else
                SoundManager.soundPlaneInstance.Volume = 0.4f;

            // Set movement restrictions for the player
            if (sprite.WorldLocation.X < 50)
            {
                moveAngle.X += 1;
            }

            if (sprite.WorldLocation.X > 750)
            {
                moveAngle.X -= 1;
            }

            if (sprite.WorldLocation.Y < 250)
            {
                moveAngle.Y += 1;
            }

            if (sprite.WorldLocation.Y > 450)
            {
                moveAngle.Y -= 1;
            }

            // If the shot button has been pressed, check if a bullet can be fired and shoot
            if (fireAngle != Vector2.Zero)
            {
                fireAngle.Normalize();

                if (WeaponManager.CanFireWeapon)
                {
                    WeaponManager.FireWeapon(
                        new Vector2(sprite.WorldLocation.X, sprite.WorldLocation.Y - 32),
                        (new Vector2(fireAngle.X + moveAngle.X * 30, fireAngle.Y * WeaponManager.WeaponSpeed)));
                }
            }

            // Move the player
            sprite.Velocity = moveAngle * 200;
        }

        #endregion

        #region Update and Draw

        public static void Update(GameTime gameTime)
        {
            CollisionManager.checkEnemyPlayerImpacts();
            CollisionManager.checkPowerupPlayerImpacts();
            handleInput(gameTime);
            sprite.Update(gameTime);
        }

        public static void Draw(SpriteBatch spriteBatch)
        {
            sprite.Draw(spriteBatch);
        }

        #endregion

    }
}
