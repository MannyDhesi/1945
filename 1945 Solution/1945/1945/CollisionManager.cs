﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace _1945
{
    class CollisionManager
    {
        #region Declarations

        static private Random rand = new Random();
        // Chance is out of 100
        static public int powerupChance = 30;
        static public int powerupRand;
        static public int powerupType;

        #endregion

        #region Rocket Splash Damage

        // Checks if an enemy is within the explosion radius when 
        // a rocket has exploded. If so, set it as Destroyed
        public static void checkRocketSplashDamage(Vector2 location)
        {
            int rocketSplashRadius = 60;
            foreach (Enemy enemy in EnemyManager.Enemies)
            {
                if (!enemy.Destroyed)
                {
                    if (enemy.sprite.IsCircleColliding(
                        location, rocketSplashRadius))
                    {
                        enemy.Destroyed = true;
                        GameManager.Score += enemy.score;
                        SoundManager.PlayExplosion1();
                        EffectsManager.AddExplosion(
                            enemy.sprite.WorldCentre,
                            Vector2.Zero);
                    }
                }
            }
            
            // If the player is too close to the explosion,
            // damage is taken
            if (Player.sprite.IsCircleColliding(
                location, rocketSplashRadius))
            {
                Player.Health -= 20;
                SoundManager.PlayExplosion1();

                EffectsManager.AddExplosion(
                    Player.sprite.WorldLocation + new Vector2(Player.sprite.FrameWidth / 4, 0),
                    Vector2.Zero);
            }

        }

        #endregion

        #region Shot - Enemy
        
        // Check for a collision between the Player's shot and the enemy
        public static void checkShotEnemyImpacts(Sprite shot, string owner)
        {
            if (shot.Expired)
            {
                return;
            }

            foreach (Enemy enemy in EnemyManager.Enemies)
            {
                if (owner.ToUpper() == "PLAYER")
                {
                    // If the enemy is not already destroyed
                    if (!enemy.Destroyed)
                    {
                        // If they have collided
                        if (shot.IsCircleColliding(
                            enemy.sprite.WorldCentre,
                            enemy.sprite.CollisionRadius))
                        {
                            // Reduce the enemy's health and increase
                            // the player's score
                            shot.Expired = true;
                            enemy.health -= 20;
                            GameManager.Score += enemy.score;

                            // If the shot type is normal, play small explosion
                            // sound and create a small explosion
                            if (shot.Frame == 0)
                            {
                                SoundManager.PlayExplosion1();
                                EffectsManager.AddExplosion(
                                    enemy.sprite.WorldCentre,
                                    enemy.sprite.Velocity / 30);
                            }

                            // If the shot type is rocket, play large explosion
                            // sound and create a large explosion
                            else if (shot.Frame == 1)
                            {
                                SoundManager.PlayExplosion2();

                                EffectsManager.AddExplosion(
                                    shot.WorldCentre,
                                    shot.Velocity / 30);

                                EffectsManager.AddExplosion(
                                            shot.WorldCentre + new Vector2(0, -10),
                                            shot.Velocity / 30);

                                EffectsManager.AddExplosion(
                                            shot.WorldCentre + new Vector2(0, 10),
                                            shot.Velocity / 30);

                                EffectsManager.AddExplosion(
                                            shot.WorldCentre + new Vector2(-10, 0),
                                            shot.Velocity / 30);

                                EffectsManager.AddExplosion(
                                            shot.WorldCentre + new Vector2(-10, 0),
                                            shot.Velocity / 30);

                                checkRocketSplashDamage(shot.WorldCentre);
                            }

                            // Check if a powerup should spawn when the enemy is destroyed
                            // Get a random number between 0 and 100
                            powerupRand = rand.Next(0, 100);

                            // If the random number is less than powerUpChance, a powerup
                            // will spawn, if it is not it will not spawn.
                            if (powerupRand < powerupChance)
                            {
                                // Get a random number between 0 and 70
                                powerupType = rand.Next(0, 70);

                                // Decides which powerup will be spawned
                                // This acts like giving each one a chance out of 70

                                if (powerupType >= 0 && powerupType < 25)
                                    WeaponManager.SpawnPowerup(
                                        enemy.sprite.WorldLocation,
                                        WeaponManager.PowerupType.Double);

                                else if (powerupType >= 25 && powerupType < 40)
                                    WeaponManager.SpawnPowerup(
                                        enemy.sprite.WorldLocation,
                                        WeaponManager.PowerupType.Triple);

                                else if (powerupType >= 40 && powerupType < 55)
                                    WeaponManager.SpawnPowerup(
                                        enemy.sprite.WorldLocation,
                                        WeaponManager.PowerupType.Rocket);

                                else if (powerupType >= 55 && powerupType < 65)
                                    WeaponManager.SpawnPowerup(
                                        enemy.sprite.WorldLocation,
                                        WeaponManager.PowerupType.Health);

                                else if (powerupType >= 65 && powerupType < 70)
                                    WeaponManager.SpawnPowerup(
                                        enemy.sprite.WorldLocation,
                                        WeaponManager.PowerupType.ExtraLife);
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region Shot - Player

        // Checks for a collision between the enemy shot and player
        public static void checkShotPlayerImpacts(Sprite shot, string owner)
        {
            if (shot.Expired)
            {
                return;
            }

            // Checking the owner makes sure the enemies can't destroy each other
            if (owner.ToUpper() == "ENEMY")
            {
                // If they have collided
                if (shot.IsCircleColliding(
                    Player.sprite.WorldCentre,
                    Player.sprite.CollisionRadius))
                {
                    // Reduce the player's health
                    shot.Expired = true;
                    Player.Health -= 20;

                    if (shot.Frame == 0)
                    {
                        // Play the explosion sound and create a small explosion
                        SoundManager.PlayExplosion1();
                        EffectsManager.AddExplosion(
                            shot.WorldCentre,
                            shot.Velocity / 30);
                    }
                }
            }
        }

        #endregion

        #region Enemy - Player

        // Check for a collision between the enemy and the player
        public static void checkEnemyPlayerImpacts()
        {
            // Go through each enemy in the Enemies List
            foreach (Enemy enemy in EnemyManager.Enemies)
            {
                // If the enemy is not Destroyed and a collision has occured
                if (!enemy.Destroyed)
                {
                    if (Player.sprite.IsCircleColliding(
                        enemy.sprite.WorldCentre,
                        enemy.sprite.CollisionRadius))
                    {
                        // Destroy the enemy and reduce the player's health
                        enemy.Destroyed = true;
                        Player.Health -= 40;

                        // Play the large explosion sound and create many explosions
                        SoundManager.PlayExplosion2();

                        EffectsManager.AddExplosion(
                                    enemy.sprite.WorldCentre,
                                    enemy.sprite.Velocity / 30);

                        EffectsManager.AddExplosion(
                                    enemy.sprite.WorldCentre + new Vector2(0, -10),
                                    enemy.sprite.Velocity / 30);

                        EffectsManager.AddExplosion(
                                    enemy.sprite.WorldCentre + new Vector2(0, 10),
                                    enemy.sprite.Velocity / 30);

                        EffectsManager.AddExplosion(
                                    enemy.sprite.WorldCentre + new Vector2(-10, 0),
                                    enemy.sprite.Velocity / 30);

                        EffectsManager.AddExplosion(
                                    enemy.sprite.WorldCentre + new Vector2(-10, 0),
                                    enemy.sprite.Velocity / 30);
                    }
                }
            }
        }

        #endregion

        #region Powerup - Player

        // Check for a collision between a powerup and the player
        public static void checkPowerupPlayerImpacts()
        {
            // Go through each item in the Powerups List
            for (int x = 0; x < WeaponManager.PowerUps.Count; x++)
            {
                // If a collision has occured
                if (Player.sprite.IsCircleColliding(
                    WeaponManager.PowerUps[x].WorldCentre,
                    WeaponManager.PowerUps[x].CollisionRadius))
                {
                    SoundManager.soundPowerupInstance.Play();

                    // Check which powerup it is and set its effects
                    switch (WeaponManager.PowerUps[x].Frame)
                    {
                        case 0:
                            WeaponManager.CurrentWeaponType = WeaponManager.WeaponType.Double;
                            WeaponManager.WeaponTimeRemaining = WeaponManager.WeaponTimeDefault;
                            break;
                        case 1:
                            WeaponManager.CurrentWeaponType = WeaponManager.WeaponType.Triple;
                            WeaponManager.WeaponTimeRemaining = WeaponManager.WeaponTimeDefault;
                            break;
                        case 2:
                            WeaponManager.rocketCount += 1;
                            break;
                        case 3:
                            Player.Health += 50;
                            if (Player.Health > 100)
                                Player.Health = 100;
                            break;
                        case 4:
                            GameManager.Lives += 1;
                            break;
                    }

                    // Remove the powerup
                    WeaponManager.PowerUps.RemoveAt(x);
                }
            }
        }

        #endregion

    }
}
