﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace _1945
{
    class EnemyManager
    {
        #region Declarations

        public static List<Enemy> Enemies = new List<Enemy>();

        public static Texture2D spriteSheet;

        private static Random rand = new Random();

        #endregion

        #region Initialization

        public static void Initialize(Texture2D texture)
        {
            // Store the spritesheet within the class
            spriteSheet = texture;
        }
        #endregion

        #region Enemy Management

        // Add a single enemy of a chosen type to the Enemies List
        
        // Adds Enemy1
        public static void AddEnemy1()
        {
            int startX = (int)rand.Next(50, 750);
            int startY = -32;

            Enemy1 newEnemy1 = new Enemy1(
                new Vector2(startX, startY),
                spriteSheet);

            Enemies.Add(newEnemy1);
        }

        // Adds Enemy2
        public static void AddEnemy2()
        {
            int startX = (int)rand.Next(50, 750);
            int startY = -32;

            Enemy2 newEnemy2 = new Enemy2(
                new Vector2(startX, startY),
                spriteSheet);

            Enemies.Add(newEnemy2);
        }

        // Adds Enemy3
        public static void AddEnemy3()
        {
            int startX = (int)rand.Next(50, 750);
            int startY = -32;

            Enemy3 newEnemy3 = new Enemy3(
                new Vector2(startX, startY),
                spriteSheet);

            Enemies.Add(newEnemy3);
        }

        // Adds Enemy4
        public static void AddEnemy4()
        {
            int startX = (int)rand.Next(50, 750);
            int startY = 632;

            Enemy4 newEnemy4 = new Enemy4(
                new Vector2(startX, startY),
                spriteSheet);

            Enemies.Add(newEnemy4);
        }

        #endregion

        #region Update and Draw

        public static void Update(GameTime gameTime)
        {
            // For each enemy in the Enemies List
            for (int x = 0; x < Enemies.Count; x++)
            {
                // Update normally
                Enemies[x].Update(gameTime);

                // If the enemy has been destroyed and none of their 
                // bullets are still on screen, remove it
                if (Enemies[x].Destroyed && !Enemies[x].Shooting)
                {
                    Enemies.RemoveAt(x);
                }
            }
        }

        public static void Draw(SpriteBatch spritebatch)
        {
            foreach (Enemy enemy in Enemies)
            {
                enemy.Draw(spritebatch);
            }
        }

        #endregion

    }
}
