﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace _1945
{
    class WeaponManager
    {
        #region Declarations

        // Normal Shooting declarations
        static public List<Particle> Shots = new List<Particle>();
        static public Texture2D Texture;
        static public Rectangle shotRectangle = new Rectangle(192, 128, 32, 32);
        static public float WeaponSpeed = 300f;
        static public float shotTimer = 0.0f;
        static public float shotMinTimer = 0.75f;

        //Weapon Upgrade declarations
        public enum WeaponType { Normal, Double, Triple };
        static public WeaponType CurrentWeaponType = WeaponType.Normal;
        static public float WeaponTimeRemaining = 10.0f;
        static public float WeaponTimeDefault = 10.0f;
        static private float tripleWeaponSplitAngle = 5;
        static public int rocketCount = 3;
        static public int rocketDistance = 100;
        static public float RocketSpeed = 200f;
        static private float rocketTimer = 0f;
        static private float rocketMinTimer = 1f;
        static private Vector2 tempPlayerLocation;

        //Power up declarations
        public enum PowerupType { Double, Triple, Rocket, Health, ExtraLife };
        static public List<Sprite> PowerUps = new List<Sprite>();
        static private int maxActivePowerups = 1;
        static private Random rand = new Random();

        #endregion

        #region Properties

        static public float WeaponFireDelay
        {
            get
            {
                return shotMinTimer;

            }
        }

        static public float RocketFireDelay
        {
            get
            {
                return rocketMinTimer;

            }
        }

        static public bool CanFireWeapon
        {
            get
            {
                return (shotTimer >= WeaponFireDelay);
            }
        }

        static public bool CanFireRocket
        {
            get
            {
                return (rocketTimer >= RocketFireDelay) && (rocketCount > 0);
            }
        }

        #endregion

        #region Effects Management Methods

        // Add a shot to the Shots List
        private static void AddShot(Vector2 location, Vector2 velocity, int frame)
        {
            Particle shot = new Particle(
                location,
                Texture,
                shotRectangle,
                velocity,
                Vector2.Zero,
                400f,
                120,
                Color.White,
                Color.White);

            shot.AddFrame(new Rectangle(224, 128, 32, 32));

            shot.Animate = false;
            shot.Frame = frame;
            shot.RotateTo(velocity);
            Shots.Add(shot);

        }

        #endregion

        #region Weapons Management Methods

        // Shoots a bullet
        public static void FireWeapon(Vector2 location, Vector2 velocity)
        {
            SoundManager.PlayShot();

            // Checks the currently active weapon type
            switch (CurrentWeaponType)
            {
                case WeaponType.Normal:
                    AddShot(location, velocity, 0);
                    break;
                case WeaponType.Double:
                    AddShot(new Vector2(location.X - 10, location.Y), velocity, 0);
                    AddShot(new Vector2(location.X + 10, location.Y), velocity, 0);
                    break;
                case WeaponType.Triple:
                    AddShot(location, velocity, 0);
                    float baseAngle = (float)Math.Atan2(velocity.Y, velocity.X);
                    float offset = MathHelper.ToRadians(tripleWeaponSplitAngle);
                    //negative offset
                    AddShot(new Vector2(location.X - 10, location.Y),
                        new Vector2((float)Math.Cos(baseAngle - offset),
                            (float)Math.Sin(baseAngle - offset)) * velocity.Length(), 0);
                    //positive offset      
                    AddShot(new Vector2(location.X + 10, location.Y),
                        new Vector2((float)Math.Cos(baseAngle + offset),
                            (float)Math.Sin(baseAngle + offset)) * velocity.Length(), 0);
                    break;
            }

            shotTimer = 0.0f;
        }

        // Shoots a rocket
        public static void FireRocket(Vector2 location, Vector2 velocity)
        {
            SoundManager.PlayRocket();
            
            tempPlayerLocation = Player.sprite.WorldLocation;
            AddShot(location, velocity, 1);
            rocketCount--;
            rocketTimer = 0.0f;
        }

        // Check if the currect weapon type has expired
        // Only occurs if a powerup has changed it from the Normal type
        private static void checkWeaponUpgradeExpire(float elapsed)
        {
            if (CurrentWeaponType != WeaponType.Normal)
            {
                WeaponTimeRemaining -= elapsed;
                if (WeaponTimeRemaining <= 0)
                {
                    CurrentWeaponType = WeaponType.Normal;
                }
            }
        }

        // Spawns a powerup at the specified location
        public static void SpawnPowerup(Vector2 location, PowerupType powerup)
        {
            if (PowerUps.Count >= maxActivePowerups)
            {
                return;
            }

            Sprite newPowerup = new Sprite(
                    location,
                    Texture,
                    new Rectangle(224, 224, 32, 32),
                    Vector2.Zero);

            newPowerup.Animate = false;
            newPowerup.CollisionRadius = 16;
            newPowerup.AddFrame(new Rectangle(256, 224, 32, 32));
            newPowerup.AddFrame(new Rectangle(288, 224, 32, 32));
            newPowerup.AddFrame(new Rectangle(320, 224, 32, 32));
            newPowerup.AddFrame(new Rectangle(352, 224, 32, 32));

            switch (powerup)
            {
                case PowerupType.Double:
                    newPowerup.Frame = 0;
                    break;
                case PowerupType.Triple:
                    newPowerup.Frame = 1;
                    break;
                case PowerupType.Rocket:
                    newPowerup.Frame = 2;
                    break;
                case PowerupType.Health:
                    newPowerup.Frame = 3;
                    break;
                case PowerupType.ExtraLife:
                    newPowerup.Frame = 4;
                    break;
            }

            PowerUps.Add(newPowerup);
        }

        #endregion

        #region Update and Draw

        static public void Update(GameTime gameTime)
        {
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;
            shotTimer += elapsed;
            rocketTimer += elapsed;
            checkWeaponUpgradeExpire(elapsed);

            // Updates each shot in the Shots List
            for (int x = 0; x < Shots.Count; x++)
            {
                Shots[x].Update(gameTime);
                CollisionManager.checkShotEnemyImpacts(Shots[x], "Player");

                // If the shot is a rocket
                if (Shots[x].Frame == 1)
                {
                    // If the rocket has travelled 100 pixels from the start position, it explodes
                    if ((tempPlayerLocation.Y - Shots[x].WorldLocation.Y - 32) > rocketDistance)
                    {
                        Shots[x].Expired = true;

                        EffectsManager.AddExplosion(
                                    Shots[x].WorldCentre,
                                    Shots[x].Velocity / 30);

                        EffectsManager.AddExplosion(
                                    Shots[x].WorldCentre + new Vector2(0, -10),
                                    Shots[x].Velocity / 30);

                        EffectsManager.AddExplosion(
                                    Shots[x].WorldCentre + new Vector2(0, 10),
                                    Shots[x].Velocity / 30);

                        EffectsManager.AddExplosion(
                                    Shots[x].WorldCentre + new Vector2(-10, 0),
                                    Shots[x].Velocity / 30);

                        EffectsManager.AddExplosion(
                                    Shots[x].WorldCentre + new Vector2(-10, 0),
                                    Shots[x].Velocity / 30);

                        CollisionManager.checkRocketSplashDamage(Shots[x].WorldCentre);
                    }
                }

                // If the shot has left the game screen, destroy it
                if (Shots[x].WorldLocation.Y < 0)
                    Shots[x].Expired = true;

                if (Shots[x].Expired)
                {
                    Shots.RemoveAt(x);
                }
            }

            // Move the powerup in the positive Y direction
            // Gives the effect it is being left behind by the player
            for (int x = 0; x < PowerUps.Count; x++)
            {
                Vector2 direction = new Vector2(0, 1);
                direction.Normalize();

                PowerUps[x].Velocity = direction *  2;
                PowerUps[x].WorldLocation += PowerUps[x].Velocity;

                // If it leaves the screen, destroy it
                if (PowerUps[x].WorldLocation.Y > 600)
                    PowerUps.RemoveAt(x);
            }
        }

        static public void Draw(SpriteBatch spriteBatch)
        {
            foreach (Particle sprite in Shots)
            {
                sprite.Draw(spriteBatch);
            }

            foreach (Sprite sprite in PowerUps)
            {
                sprite.Draw(spriteBatch);
            }
        }

        #endregion
    }
}
