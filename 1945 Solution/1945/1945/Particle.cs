﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace _1945
{
    class Particle : Sprite
    {
        #region Declarations

        private Vector2 acceleration;
        private float maxSpeed;
        private int initialDuration;
        private int remainingDuration;
        private Color initialColour;
        private Color finalColour;

        #endregion

        #region Properties

        // Returns the difference between initialDuration and remainingDuration
        // Results in the number of frames for which the particle will continue to exist
        public int ElapsedDuration
        {
            get
            {
                return initialDuration - remainingDuration;
            }
        }

        // A & value between 0 and 1 is returned
        public float DurationProgress
        {
            get
            {
                return (float)ElapsedDuration / (float)initialDuration;
            }
        }

        // Returns false if the remainingDuration = 0
        public bool IsActive
        {
            get
            {
                return (remainingDuration > 0);
            }
        }

        #endregion

        #region Constructor

        // Extends the Sprite class, adding new features to those of its parent
        public Particle(Vector2 location, Texture2D texture, Rectangle initialFrame,
            Vector2 velocity, Vector2 acceleration, float maxSpeed, int duration,
            Color initialColour, Color finalColour)
            : base(location, texture, initialFrame, velocity)
        {
            initialDuration = duration;
            remainingDuration = duration;
            this.acceleration = acceleration;
            this.initialColour = initialColour;
            this.maxSpeed = maxSpeed;
            this.finalColour = finalColour;
        }

        #endregion

        #region Update and Draw

        // Both of these override the Sprite class's methods of the same name
        // and will only execute any code if the IsActive property is true.

        public override void Update(GameTime gameTime)
        {
            if (IsActive)
            {
                Velocity += acceleration;

                // If the length of the velocity vector has exceeded the maximum speed,
                // the velocity vector is normalized and multiplied by the max speed,
                // resulting in a vector pointing in the same direction at the max length
                if (Velocity.Length() > maxSpeed)
                {
                    Velocity.Normalize();
                    Velocity *= maxSpeed;
                }

                // Lerp function gradually changes from one colour to the other
                TintColour = Color.Lerp(initialColour, finalColour, DurationProgress);
                remainingDuration--;
            }

            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (IsActive)
            {
                base.Draw(spriteBatch);
            }
        }

        #endregion
    }
}
