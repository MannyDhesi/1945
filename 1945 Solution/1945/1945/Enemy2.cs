﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace _1945
{
    class Enemy2 : Enemy
    {
        #region Declarations

        public List<Particle> Shots = new List<Particle>();
        static public Texture2D Texture;
        static public Rectangle shotRectangle = new Rectangle(128, 64, 32, 32);
        static public Rectangle initialFrame = new Rectangle(0, 32, 32, 32);
        public float WeaponSpeed = 150f;
        private float shotTimer = 0f;
        private float shotMinTimer = 2f;

        #endregion

        #region Constructor

        public Enemy2(Vector2 worldLocation,
            Texture2D texture)
        {
            sprite = new Sprite(
               worldLocation,
               texture,
               initialFrame,
               Vector2.Zero);

            speed = 100f;
            health = 20;
            score = 20;

            Texture = texture;
            sprite.AnimateWhenStopped = true;
            sprite.CollisionRadius = 16;

            // Initialize the size of the sprite
            int frameWidth = initialFrame.Width;
            int frameHeight = initialFrame.Height;

            // Add each frame for the animation of the base sprite
            for (int x = 1; x < 3; x++)
            {
                // Sets a new rectangle to encapsulate the tank base so everytime we move
                // to the next frame (x++) the new rectangle will be pushed to the new (X,Y).
                // On the sprite sheet, the tank sprites are set up along the same Y value
                // which is why the baseInitialFrame.Y remains the same for all frames
                sprite.AddFrame(new Rectangle(initialFrame.X + (frameHeight * x),
                    initialFrame.Y, frameWidth, frameHeight));
            }
        }

        #endregion

        #region Shooting

        // Function to create a shot and add it to the List called Shots
        private void AddShot(Vector2 location, Vector2 velocity, int frame)
        {
            Particle shot = new Particle(
                location,
                Texture,
                shotRectangle,
                velocity,
                Vector2.Zero,
                400f,
                200,
                Color.White,
                Color.White);

            shot.AddFrame(shotRectangle);

            shot.Animate = false;
            shot.Frame = frame;
            shot.RotateTo(velocity);
            Shots.Add(shot);

        }

        #endregion

        #region Public Methods

        public override void Update(GameTime gameTime)
        {
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;
            
            // Update each shot in the Shots List
            for (int x = Shots.Count - 1; x >= 0; x--)
            {
                Shots[x].Update(gameTime);
                // Check for a collision between the bullet and player
                CollisionManager.checkShotPlayerImpacts(Shots[x], "Enemy");

                // If the shot leaves the screen, set it as expired
                if (Shots[x].WorldLocation.Y > 600)
                    Shots[x].Expired = true;

                // If the shot has expired, remove it
                if (Shots[x].Expired)
                {
                    Shots.RemoveAt(x);
                }
            }

            // If the enemy is not destroyed
            if (!Destroyed)
            {
                // Move their plane in the velocity's direction
                Vector2 direction = new Vector2(0, 1);
                direction.Normalize();

                sprite.Velocity = direction * speed;
                sprite.RotateTo(direction);

                // If their plane has left the screen, destroy it
                if (sprite.WorldLocation.Y > 600)
                    Destroyed = true;

                shotTimer += elapsed;

                // If the set time has passed since the last shot was fired
                if (shotTimer >= shotMinTimer)
                {
                    SoundManager.PlayShot();

                    // Fire a shot straight forward
                    Vector2 fireAngle = new Vector2(0, 1);
                    fireAngle.Normalize();

                    AddShot(new Vector2(sprite.WorldLocation.X, sprite.WorldLocation.Y + 16),
                            new Vector2(fireAngle.X, fireAngle.Y * WeaponSpeed),
                            0);

                    Shooting = true;

                    shotTimer = 0.0f;
                }
            }

            if (Shots.Count == 0)
                Shooting = false;

            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            foreach (Particle sprite in Shots)
            {
                sprite.Draw(spriteBatch);
            }

            base.Draw(spriteBatch);
        }

        #endregion
    }
}
