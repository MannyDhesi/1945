using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace _1945
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        #region Declarations

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Texture2D titleScreen;
        Texture2D spriteSheet;
        Texture2D waterBackground;
        SpriteFont Arial14;
        SpriteFont Arial20;

        enum GameStates
        {
            TitleScreen,
            StartGame,
            Playing,
            WaveComplete,
            LostLife,
            GameOver,
            Pause
        };

        GameStates currentState = GameStates.TitleScreen;
        float lostLifeTimer = 0.0f;
        float lostLifeDelay = 3.0f;
        float gameOverTimer = 0.0f;
        float gameOverDelay = 3.0f;

        KeyboardState oldKeyState;
        ButtonState oldButtonState;
        KeyboardState newKeyState;
        ButtonState newButtonState;

        KeyboardState oldKeyState1;
        ButtonState oldButtonState1;
        KeyboardState newKeyState1;
        ButtonState newButtonState1;

        KeyboardState newDebugKeyState;
        KeyboardState oldDebugKeyState;

        bool isDebugging = false;

        #endregion

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            this.graphics.PreferredBackBufferWidth = 800;
            this.graphics.PreferredBackBufferHeight = 600;
            this.graphics.ApplyChanges();

            this.IsMouseVisible = true;
            base.Initialize();
        }

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // Load the Sprite Sheets and Fonts
            titleScreen = Content.Load<Texture2D>("Textures/TitleScreen");
            spriteSheet = Content.Load<Texture2D>("Textures/SpriteSheet");
            waterBackground = Content.Load<Texture2D>("Textures/WaterBackground");
            Arial14 = Content.Load<SpriteFont>("Fonts/Arial14");
            Arial20 = Content.Load<SpriteFont>("Fonts/Arial20");

            SoundManager.soundExplosion1 = Content.Load<SoundEffect>("Sounds/snd_explosion1");
            SoundManager.soundExplosion2 = Content.Load<SoundEffect>("Sounds/snd_explosion2");
            SoundManager.soundShot = Content.Load<SoundEffect>("Sounds/snd_shot");
            SoundManager.soundRocket = Content.Load<SoundEffect>("Sounds/snd_rocket");
            SoundManager.soundPlane = Content.Load<SoundEffect>("Sounds/snd_plane");
            SoundManager.soundPowerup = Content.Load<SoundEffect>("Sounds/snd_powerup");
            SoundManager.soundGameOver = Content.Load<SoundEffect>("Sounds/snd_gameover");

            SoundManager.Initialize();

            Background.Initialize(waterBackground, spriteSheet);

            // Construct the Player's plane in position (100,100) using the spritesheet
            // texture, with initial frame from the tiles with coordinates (0,64) and
            // width & height set to 32 pixels
            Player.Initialize(spriteSheet,
                new Rectangle(0, 128, 64, 64),
                3,
                new Vector2(400, 350));

            // 
            EffectsManager.Initialize(
                spriteSheet,
                new Rectangle(288, 160, 2, 2),
                new Rectangle(288, 128, 32, 32),
                3);

            WeaponManager.Texture = spriteSheet;

            EnemyManager.Initialize(spriteSheet);

            HUD.Initialize(spriteSheet);
        }

        protected override void UnloadContent()
        {

        }

        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            SoundManager.Update(gameTime);

            // Check what the current state is and call the correct Update functions
            switch (currentState)
            {
                case GameStates.TitleScreen:
                    // Procedure to make sure key is pressed and released (pressed once)
                    // If pause button is pressed, pause the game
                    newKeyState = Keyboard.GetState();
                    newButtonState = GamePad.GetState(PlayerIndex.One).Buttons.Start;

                    // If Space or A is pressed, start the game
                    if ((GamePad.GetState(PlayerIndex.One).Buttons.A == ButtonState.Pressed) ||
                        (Keyboard.GetState().IsKeyDown(Keys.Space)))
                    {
                        GameManager.StartNewGame();
                        currentState = GameStates.Playing;
                    }

                    oldKeyState = newKeyState;
                    oldButtonState = newButtonState;

                    break;

                case GameStates.StartGame:
                    // Start a new game
                    GameManager.StartNewGame();
                    currentState = GameStates.Playing;
                    break;

                case GameStates.Playing:
                    // Update the game normally during gameplay
                    Background.Update(gameTime);
                    Player.Update(gameTime);
                    WeaponManager.Update(gameTime);
                    EnemyManager.Update(gameTime);
                    EffectsManager.Update(gameTime);
                    EnemySpawner.Update(gameTime);

                    // If the player has died
                    if (Player.Health <= 0)
                    {
                        // Play explosion sound and deduct a life
                        SoundManager.soundExplosion2.Play();
                        Player.Health = 0;
                        GameManager.Lives -= 1;

                        // If the player has lives remaining, got to lost life state
                        if (GameManager.Lives > 0)
                        {
                            currentState = GameStates.LostLife;
                        }

                        // Else if no lives remaining, go to game over state
                        else
                        {
                            currentState = GameStates.GameOver;
                        }
                    }

                    // Procedure to make sure key is pressed and released (pressed once)
                    // If pause button is pressed, pause the game
                    newKeyState = Keyboard.GetState();
                    newButtonState = GamePad.GetState(PlayerIndex.One).Buttons.Start;

                    if ((newKeyState.IsKeyDown(Keys.P) && oldKeyState.IsKeyUp(Keys.P)) ||
                        (newButtonState == ButtonState.Pressed && oldButtonState == ButtonState.Released))
                    {
                        currentState = GameStates.Pause;
                        SoundManager.PauseActiveSounds();
                    }

                    oldKeyState = newKeyState;
                    oldButtonState = newButtonState;

                    break;

                case GameStates.WaveComplete:
                    break;

                case GameStates.LostLife:
                    // Show lost life screen for set time then return to the game
                    EffectsManager.Update(gameTime);
                    lostLifeTimer += (float)gameTime.ElapsedGameTime.TotalSeconds;
                    if (lostLifeTimer > lostLifeDelay)
                    {
                        GameManager.RespawnPlayer();
                        currentState = GameStates.Playing;
                        lostLifeTimer = 0.0f;
                    }
                    break;

                case GameStates.GameOver:
                    SoundManager.soundGameOverInstance.Play();
                    // Show game over screen for set time then return to title screen
                    EffectsManager.Update(gameTime);
                    gameOverTimer += (float)gameTime.ElapsedGameTime.TotalSeconds;
                    if (gameOverTimer > gameOverDelay)
                    {
                        SoundManager.StopActiveSounds();
                        currentState = GameStates.TitleScreen;
                        gameOverTimer = 0.0f;
                    }
                    break;

                case GameStates.Pause:
                    // Procedure to make sure key is pressed and released (pressed once)
                    // If pause button is pressed, resume the game
                    newKeyState = Keyboard.GetState();
                    newButtonState = GamePad.GetState(PlayerIndex.One).Buttons.Start;

                    if ((newKeyState.IsKeyDown(Keys.P) && oldKeyState.IsKeyUp(Keys.P)) ||
                        (newButtonState == ButtonState.Pressed && oldButtonState == ButtonState.Released))
                    {
                        currentState = GameStates.Playing;
                        SoundManager.ResumeActiveSounds();
                    }

                    oldKeyState = newKeyState;
                    oldButtonState = newButtonState;

                    newKeyState1 = Keyboard.GetState();
                    newButtonState1 = GamePad.GetState(PlayerIndex.One).Buttons.B;

                    if ((newKeyState1.IsKeyDown(Keys.Escape) && oldKeyState1.IsKeyUp(Keys.Escape)) ||
                        (newButtonState1 == ButtonState.Pressed && oldButtonState1 == ButtonState.Released))
                    {
                        currentState = GameStates.TitleScreen;
                        SoundManager.StopActiveSounds();
                    }

                    oldKeyState1 = newKeyState1;
                    oldButtonState1 = newButtonState1;
                    break;
            }

            // Display debugging information (current state and FPS) when F1 is pressed
            newDebugKeyState = Keyboard.GetState();

            if (newDebugKeyState.IsKeyDown(Keys.F1) && oldDebugKeyState.IsKeyUp(Keys.F1))
            {
                if (isDebugging == false)
                    isDebugging = true;

                else if (isDebugging == true)
                    isDebugging = false;
            }

            oldDebugKeyState = newDebugKeyState;

            
            base.Update(gameTime);

        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();

            // Check what the current state is and call the correct Draw functions
            switch (currentState)
            {
                case GameStates.TitleScreen:
                    spriteBatch.Draw(
                    titleScreen,
                    new Rectangle(0, 0, 800, 600),
                    Color.White);
                    break;

                case GameStates.StartGame:
                case GameStates.Playing:
                case GameStates.WaveComplete:
                case GameStates.LostLife:
                case GameStates.GameOver:
                    Background.Draw(spriteBatch);
                    Player.Draw(spriteBatch);
                    EnemyManager.Draw(spriteBatch);
                    WeaponManager.Draw(spriteBatch);
                    EffectsManager.Draw(spriteBatch);
                    HUD.Draw(spriteBatch, Arial14, Arial20);

                    // Display HUD info when player loses a life
                    if (currentState == GameStates.LostLife)
                    {
                        // Draw the background
                        spriteBatch.Draw(spriteSheet, new Rectangle(224, 236, 352, 128), new Rectangle(0, 384, 352, 128), Color.White);

                        // Draw the text on the screen
                        spriteBatch.DrawString(
                            Arial20,
                            "L I F E    L O S T !",
                            new Vector2(400, 280),
                            Color.White,
                            0f,
                            Arial20.MeasureString("L I F E    L O S T !") / 2,
                            1f,
                            SpriteEffects.None,
                            0f);

                        if (GameManager.Lives > 1)
                            spriteBatch.DrawString(
                                Arial20,
                                GameManager.Lives.ToString() + "    L I V E S    L E F T !",
                                new Vector2(400, 320),
                                Color.White,
                                0f,
                                Arial20.MeasureString(GameManager.Lives.ToString() + "    L I V E S    L E F T !") / 2,
                                1f,
                                SpriteEffects.None,
                                0f);

                        else
                            spriteBatch.DrawString(
                                Arial20,
                                GameManager.Lives.ToString() + "    L I F E    L E F T !",
                                new Vector2(400, 320),
                                Color.White,
                                0f,
                                Arial20.MeasureString(GameManager.Lives.ToString() + "    L I F E    L E F T !") / 2,
                                1f,
                                SpriteEffects.None,
                                0f);
                    }

                    // Draw HUD info when player has run out of lives (Game Over)
                    if (currentState == GameStates.GameOver)
                    {
                        // Draw the background
                        spriteBatch.Draw(spriteSheet, new Rectangle(224, 236, 352, 128), new Rectangle(0, 384, 352, 128), Color.White);

                        // Draw the text on the screen
                        spriteBatch.DrawString(
                            Arial20,
                            "G A M E    O V E R !",
                            new Vector2(400, 280),
                            Color.White,
                            0f,
                            Arial20.MeasureString("G A M E    O V E R !") / 2,
                            1f,
                            SpriteEffects.None,
                            0f);

                        spriteBatch.DrawString(
                            Arial20,
                            "S C O R E :    " + GameManager.Score.ToString(),
                            new Vector2(400, 320),
                            Color.White,
                            0f,
                            Arial20.MeasureString("S C O R E :    " + GameManager.Score.ToString()) / 2,
                            1f,
                            SpriteEffects.None,
                            0f);
                    }
                    break;

                case GameStates.Pause:
                    Background.Draw(spriteBatch);
                    Player.Draw(spriteBatch);
                    EnemyManager.Draw(spriteBatch);
                    WeaponManager.Draw(spriteBatch);
                    EffectsManager.Draw(spriteBatch);
                    HUD.Draw(spriteBatch, Arial14, Arial20);

                    // Draw the background
                    spriteBatch.Draw(spriteSheet, new Rectangle(224, 236, 352, 128), new Rectangle(352, 384, 352, 128), Color.White);

                    // Draw the text on the screen
                    spriteBatch.DrawString(
                        Arial20,
                        "G A M E    P A U S E D !",
                        new Vector2(400, 275),
                        Color.White,
                        0f,
                        Arial20.MeasureString("G A M E    P A U S E D !") / 2,
                        1f,
                        SpriteEffects.None,
                        0f);

                    break;
            }

            // Draw the current game state and FPS in the top-right corner
            if (isDebugging)
            {
                float frameRate = 1 / (float)gameTime.ElapsedGameTime.TotalSeconds;
                spriteBatch.DrawString(
                    Arial14,
                    "State: " + currentState.ToString() + "\nFPS: " + frameRate,
                    new Vector2(5, 5),
                    Color.White);
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
