﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace _1945
{
    static class GameManager
    {
        #region Declarations

        public static int Score = 0;
        public static int Lives = 3;

        #endregion

        #region Public Methods

        // Reset the variables for a new game
        public static void StartNewGame()
        {
            Lives = 3;
            Score = 0;
            WeaponManager.rocketCount = 3;
            WeaponManager.shotTimer = 0.0f;

            EnemySpawner.MaxActiveEnemies = 1;
            EnemySpawner.CurrentEnemyLevel = 1;

            SoundManager.soundPlaneInstance.Play();

            RespawnPlayer();
        }

        // Clear everything on screen and reset the player
        public static void RespawnPlayer()
        {
            Player.Health = 100;
            Player.sprite.WorldLocation = new Vector2(400, 350);
            WeaponManager.Shots.Clear();
            WeaponManager.PowerUps.Clear();
            EffectsManager.Effects.Clear();
            WeaponManager.CurrentWeaponType = WeaponManager.WeaponType.Normal;
            EnemyManager.Enemies.Clear();
        }

        #endregion
    }
}
