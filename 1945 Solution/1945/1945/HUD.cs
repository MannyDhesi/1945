﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace _1945
{
    static class HUD
    {
        #region Declarations

        public static Rectangle background;
        public static Texture2D texture;

        #endregion

        #region Initialization

        public static void Initialize(Texture2D spriteSheet)
        {
            background = new Rectangle(0, 504, 800, 96);
            texture = spriteSheet;
        }

        #endregion

        #region Public Methods

        public static void Draw(SpriteBatch spriteBatch, SpriteFont Arial14, SpriteFont Arial20)
        {
            // Draw the HUD Background
            spriteBatch.Draw(texture, background, new Rectangle(0, 288, 800, 96), Color.White);

            // Draw the Health Bar
            spriteBatch.Draw(texture, new Vector2(30, 545), new Rectangle(0, 224, Player.Health * 2, 32), Color.White);
            spriteBatch.Draw(texture, new Vector2(30, 545), new Rectangle(0, 256, 200, 32), Color.White);

            // Draw the text on the HUD
            spriteBatch.DrawString(
                Arial20,
                "HEALTH",
                new Vector2(130, 530),
                Color.White,
                0f,
                Arial20.MeasureString("HEALTH") / 2,
                1f,
                SpriteEffects.None,
                0f);

            // Draw the Player's Score
            spriteBatch.DrawString(
                Arial14,
                "SCORE",
                new Vector2(300, 530),
                Color.White,
                0f,
                Arial14.MeasureString("SCORE") / 2,
                1f,
                SpriteEffects.None,
                0f);

            spriteBatch.DrawString(
                Arial20,
                GameManager.Score.ToString(),
                new Vector2(300, 560),
                Color.White,
                0f,
                Arial20.MeasureString(GameManager.Score.ToString()) / 2,
                1f,
                SpriteEffects.None,
                0f);

            // Draw how many lives are left
            spriteBatch.DrawString(
                Arial14,
                "LIVES",
                new Vector2(400, 530),
                Color.White,
                0f,
                Arial14.MeasureString("LIVES") / 2,
                1f,
                SpriteEffects.None,
                0f);

            spriteBatch.Draw(texture,
                new Rectangle(365, 545, 32, 32),
                new Rectangle(352, 224, 32, 32),
                Color.White);

            spriteBatch.DrawString(
                Arial14,
                "x " + GameManager.Lives.ToString(),
                new Vector2(405, 561),
                Color.White,
                0f,
                new Vector2(0, Arial14.MeasureString("x " + GameManager.Lives.ToString()).Y / 2),
                1f,
                SpriteEffects.None,
                0f);

            // Draw the type of bullet in use
            spriteBatch.DrawString(
                Arial14,
                "BULLETS",
                new Vector2(550, 530),
                Color.White,
                0f,
                Arial14.MeasureString("BULLETS") / 2,
                1f,
                SpriteEffects.None,
                0f);

            spriteBatch.Draw(texture,
                new Rectangle(469, 545, 32, 32),
                new Rectangle(288, 224, 32, 32),
                Color.White);

            spriteBatch.DrawString(
                Arial14,
                "x " + WeaponManager.rocketCount.ToString(),
                new Vector2(505, 561),
                Color.White,
                0f,
                new Vector2(0, Arial14.MeasureString("x " + WeaponManager.rocketCount.ToString()).Y / 2),
                1f,
                SpriteEffects.None,
                0f);

            switch (WeaponManager.CurrentWeaponType)
            {
                case WeaponManager.WeaponType.Normal:
                    spriteBatch.Draw(texture,
                        new Rectangle(575, 545, 32, 32),
                        new Rectangle(224, 192, 32, 32),
                        Color.White);
                    break;
                case WeaponManager.WeaponType.Double:
                    spriteBatch.Draw(texture,
                        new Rectangle(565, 545, 32, 32),
                        new Rectangle(224, 224, 32, 32),
                        Color.White);
                    break;
                case WeaponManager.WeaponType.Triple:
                    spriteBatch.Draw(texture,
                        new Rectangle(565, 545, 32, 32),
                        new Rectangle(256, 224, 32, 32),
                        Color.White);
                    break;
            }

            if (WeaponManager.CurrentWeaponType != WeaponManager.WeaponType.Normal)
            {
                spriteBatch.DrawString(
                    Arial14,
                    ((int)WeaponManager.WeaponTimeRemaining + 1).ToString() + "s",
                    new Vector2(598, 561),
                    Color.White,
                    0f,
                    new Vector2(0, Arial14.MeasureString(((int)WeaponManager.WeaponTimeRemaining + 1).ToString() + "s").Y / 2),
                    1f,
                    SpriteEffects.None,
                    0f);
            }
        }

        #endregion

    }
}
