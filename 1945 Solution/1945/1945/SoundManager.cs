﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace _1945
{
    static class SoundManager
    {
        #region Declarations

        public static SoundEffect soundExplosion1;
        public static SoundEffect soundExplosion2;
        public static SoundEffect soundShot;
        public static SoundEffect soundRocket;
        public static SoundEffect soundPlane;
        public static SoundEffect soundPowerup;
        public static SoundEffect soundGameOver;

        public static List<SoundEffectInstance> SoundExplosion1Instances = new List<SoundEffectInstance>();
        public static List<SoundEffectInstance> SoundExplosion2Instances = new List<SoundEffectInstance>();
        public static List<SoundEffectInstance> SoundShotInstances = new List<SoundEffectInstance>();
        public static List<SoundEffectInstance> SoundRocketInstances = new List<SoundEffectInstance>();

        public static SoundEffectInstance soundPlaneInstance;
        public static SoundEffectInstance soundPowerupInstance;
        public static SoundEffectInstance soundGameOverInstance;

        #endregion

        #region Public Methods

        public static void Initialize()
        {
            SoundManager.soundPlaneInstance = SoundManager.soundPlane.CreateInstance();
            SoundManager.soundPowerupInstance = SoundManager.soundPowerup.CreateInstance();
            SoundManager.soundGameOverInstance = SoundManager.soundExplosion2.CreateInstance();

            SoundManager.soundPlaneInstance.IsLooped = true;
            SoundManager.soundPlaneInstance.Volume = 0.5f;
        }

        public static void PauseActiveSounds()
        {
            for (int i = 0; i < SoundExplosion1Instances.Count; i++)
            {
                if (SoundExplosion1Instances[i].State == SoundState.Playing)
                    SoundExplosion1Instances[i].Pause();
            }

            for (int i = 0; i < SoundExplosion2Instances.Count; i++)
            {
                if (SoundExplosion2Instances[i].State == SoundState.Playing)
                    SoundExplosion2Instances[i].Pause();
            }

            for (int i = 0; i < SoundShotInstances.Count; i++)
            {
                if (SoundShotInstances[i].State == SoundState.Playing)
                    SoundShotInstances[i].Pause();
            }

            for (int i = 0; i < SoundRocketInstances.Count; i++)
            {
                if (SoundRocketInstances[i].State == SoundState.Playing)
                    SoundRocketInstances[i].Pause();
            }

            if (soundPlaneInstance.State == SoundState.Playing)
                soundPlaneInstance.Pause();

            if (soundPowerupInstance.State == SoundState.Playing)
                soundPowerupInstance.Pause();

            if (soundGameOverInstance.State == SoundState.Playing)
                soundGameOverInstance.Pause();
        }

        public static void ResumeActiveSounds()
        {
            for (int i = 0; i < SoundExplosion1Instances.Count; i++)
            {
                if (SoundExplosion1Instances[i].State == SoundState.Paused)
                    SoundExplosion1Instances[i].Resume();
            }

            for (int i = 0; i < SoundExplosion2Instances.Count; i++)
            {
                if (SoundExplosion2Instances[i].State == SoundState.Paused)
                    SoundExplosion2Instances[i].Resume();
            }

            for (int i = 0; i < SoundShotInstances.Count; i++)
            {
                if (SoundShotInstances[i].State == SoundState.Paused)
                    SoundShotInstances[i].Resume();
            }

            for (int i = 0; i < SoundRocketInstances.Count; i++)
            {
                if (SoundRocketInstances[i].State == SoundState.Paused)
                    SoundRocketInstances[i].Resume();
            }

            if (soundPlaneInstance.State == SoundState.Paused)
                soundPlaneInstance.Resume();

            if (soundPowerupInstance.State == SoundState.Paused)
                soundPowerupInstance.Resume();

            if (soundGameOverInstance.State == SoundState.Paused)
                soundGameOverInstance.Resume();
        }

        public static void StopActiveSounds()
        {
            for (int i = 0; i < SoundExplosion1Instances.Count; i++)
            {
                SoundExplosion1Instances[i].Stop();
                SoundExplosion1Instances.RemoveAt(i);
            }

            for (int i = 0; i < SoundExplosion2Instances.Count; i++)
            {
                SoundExplosion2Instances[i].Stop();
                SoundExplosion1Instances.RemoveAt(i);
            }

            for (int i = 0; i < SoundShotInstances.Count; i++)
            {
                SoundShotInstances[i].Stop();
                SoundShotInstances.RemoveAt(i);
            }

            for (int i = 0; i < SoundRocketInstances.Count; i++)
            {
                SoundRocketInstances[i].Stop();
                SoundRocketInstances.RemoveAt(i);
            }

            soundPlaneInstance.Stop();

            soundPowerupInstance.Stop();

            soundGameOverInstance.Stop();
        }

        public static void PlayExplosion1()
        {
            SoundEffectInstance newInstance = soundExplosion1.CreateInstance();

            SoundExplosion1Instances.Add(newInstance);

            SoundExplosion1Instances[SoundExplosion1Instances.Count - 1].Play();
        }

        public static void PlayExplosion2()
        {
            SoundEffectInstance newInstance = soundExplosion2.CreateInstance();

            SoundExplosion2Instances.Add(newInstance);

            SoundExplosion2Instances[SoundExplosion2Instances.Count - 1].Play();
        }

        public static void PlayShot()
        {
            SoundEffectInstance newInstance = soundShot.CreateInstance();

            SoundShotInstances.Add(newInstance);

            SoundShotInstances[SoundShotInstances.Count - 1].Play();
        }

        public static void PlayRocket()
        {
            SoundEffectInstance newInstance = soundRocket.CreateInstance();

            SoundRocketInstances.Add(newInstance);

            SoundRocketInstances[SoundRocketInstances.Count - 1].Play();
        }

        public static void Update(GameTime gameTime)
        {
            for (int i = 0; i < SoundExplosion1Instances.Count; i++)
            {
                SoundExplosion1Instances[i].Volume = 0.6f;

                if (SoundExplosion1Instances[i].State == SoundState.Stopped)
                    SoundExplosion1Instances.RemoveAt(i);
            }

            for (int i = 0; i < SoundExplosion2Instances.Count; i++)
            {
                SoundExplosion2Instances[i].Volume = 0.6f;

                if (SoundExplosion2Instances[i].State == SoundState.Stopped)
                    SoundExplosion2Instances.RemoveAt(i);
            }

            for (int i = 0; i < SoundShotInstances.Count; i++)
            {
                SoundShotInstances[i].Volume = 0.6f;

                if (SoundShotInstances[i].State == SoundState.Stopped)
                    SoundShotInstances.RemoveAt(i);
            }

            for (int i = 0; i < SoundRocketInstances.Count; i++)
            {
                SoundRocketInstances[i].Volume = 0.6f;

                if (SoundRocketInstances[i].State == SoundState.Stopped)
                    SoundRocketInstances.RemoveAt(i);
            }
        }

        #endregion
    }
}
