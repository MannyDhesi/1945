﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace _1945
{
    abstract class Enemy
    {
        #region Declarations

        public Sprite sprite;
        public float speed;
        public int health;
        public int score;
        public bool Destroyed = false;
        public bool Shooting = false;

        #endregion

        #region Public Methods

        public virtual void Update(GameTime gameTime)
        {
            // If the enemy is not destroyed, check it's health and update normally
            if (!Destroyed)
            {
                if (health <= 0)
                    Destroyed = true;

                sprite.Update(gameTime);
            }
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            // If the enemy is not destroyed, draw it normally
            if (!Destroyed)
            {
                sprite.Draw(spriteBatch);
            }
        }

        #endregion
    }
}
